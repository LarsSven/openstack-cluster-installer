class oci::puppetserver(
  $mysql_root_password = undef,
  $mysql_oci_password  = undef,
){

  ###########################
  ### First, setup the DB ###
  ###########################

  include mysql::server
  include mysql::client

  $ram_total_in_mb = Integer($facts['memorysize_mb'])
  $ram_total_in_gb = $ram_total_in_mb / 1024
  $ram_total_in_gb_div_4 = $ram_total_in_gb / 4

  if $ram_total_in_gb_div_4 > 64 {
    $innodb_buffer_pool_size = 64
  }elsif $ram_total_in_gb_div_4 > 32{
    $innodb_buffer_pool_size = 32
  }elsif $ram_total_in_gb_div_4 > 16{
    $innodb_buffer_pool_size = 16
  }elsif $ram_total_in_gb_div_4 > 8{
    $innodb_buffer_pool_size = 8
  }elsif $ram_total_in_gb_div_4 > 4{
    $innodb_buffer_pool_size = 4
  }else{
    $innodb_buffer_pool_size = 2
  }

  class { 'mysql::server':
    root_password => $mysql_root_password,
    override_options => {
      'mysqld' => {
        'bind_address'                    => '127.0.0.1',
        'wait_timeout'                    => '28800',
        'interactive_timeout'             => '30',
        'connect_timeout'                 => '30',
        'character_set_server'            => 'utf8',
        'collation_server'                => 'utf8_general_ci',
        'innodb_buffer_pool_size'         => "${innodb_buffer_pool_size}G",
        'innodb_flush_log_at_trx_commit'  => '2',
        'max_connections'                 => '5000',
        'max_user_connections'            => '1000',
        'binlog_cache_size'               => '1M',
        'log-bin'                         => 'mysql-bin',
        'binlog_format'                   => 'ROW',
        'performance_schema'              => '1',
        'log_warnings'                    => '2',
      }
    }
  }
  -> ::openstacklib::db::mysql { 'oci':
    user          => 'oci',
    password      => $mysql_oci_password,
    dbname        => 'oci',
    allowed_hosts => '%',
  }

  ################################################
  ### Setup the OCI package and do the db sync ###
  ################################################
  package { 'openstack-cluster-installer':
    ensure => 'present',
  }
  -> file_line { 'sql-connection-oci':
    path    => '/etc/openstack-cluster-installer/openstack-cluster-installer.conf',
    match   => '.*connection.*=.*',
    line    => "connection=mysql+pymysql://oci:${mysql_oci_password}@localhost:3306/oci",
  }
  -> package { 'php-cli':
    ensure => 'present',
  }
  -> exec { 'oci-db-sync':
    creates => '/var/lib/mysql/oci/clusters.MYD',
    path    => ['/usr/bin', '/usr/sbin', '/bin', '/sbin', ],
    command => "/bin/true # comment to satisfy puppet syntax requirements
set -ex

cd /usr/share/openstack-cluster-installer
php db_sync.php"
  }

  include ::apache::params
  class { '::apache': }
  class { 'apache::mod::ssl': }

}
