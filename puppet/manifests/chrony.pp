class oci::chrony(
  $time_server_host = "0.debian.pool.ntp.org"
){
  if Integer($facts['os']['release']['major']) >= 12 {
    # puppet-module-aboe-chrony >= 3, which is
    # available on osbpo.debian.net for >= bookworm
    class { '::chrony':
      pools            => split($time_server_host, ';'),
      servers          => [],
      makestep_seconds => 120,
      makestep_updates => -1,
      log_options      => 'tracking measurements statistics',
    }
  } else {
    # puppet-module-aboe-chrony < 3
    class { '::chrony':
      servers          => split($time_server_host, ';'),
      makestep_seconds => '120',
      makestep_updates => '-1',
      log_options      => 'tracking measurements statistics',
    }
  }
}
