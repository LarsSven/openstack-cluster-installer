#!/bin/sh

set -e

if ! [ -x /usr/bin/disk-firmware-updater ] ; then
	echo "/usr/bin/disk-firmware-updater not present: exiting."
	exit 1
fi

echo "===> Stop all services"
/etc/init.d/puppet stop
for i in /etc/init.d/swift-object* ; do $i stop ; done
/etc/init.d/rsync stop

echo "===> unmount all disks"
for i in $(cat /proc/mounts | grep srv/node | awk '{print $2}') ; do echo "-> Unmounting $i" ; umount $i ; done

echo "===> Erasing any data in the /"
rm -rf /srv/node/sd*/objects

echo "===> Fixing /srv/node/sd* unix rights to root:root"
chown root:root /srv/node/sd*

echo "===> Upgrading all disks firmware"
for i in /dev/sd* ; do
	TO_UP=$(basename $i)
	echo "-> Starting updater on $i"
	disk-firmware-updater ${TO_UP}
done

echo "===> Fixing fstab"
sed -i 's/#UUID=/UUID=/' /etc/fstab 

echo "===> Remounting all disks"
for i in $(ls -d /srv/node/sd*) ; do echo "-> Mounting $i" ; mount $i ; done

echo "===> Restarting all services"
/etc/init.d/rsync start
for i in /etc/init.d/swift-object* ; do $i start ; done
/etc/init.d/puppet start
