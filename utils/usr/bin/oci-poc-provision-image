#!/bin/sh

set -e
#set -x

. /root/oci-openrc

DEBIAN_RELEASE=$(lsb_release -c -s)
DEBIAN_RELEASE_NUM=$(lsb_release -r -s)
DEBIAN_ARCH=$(dpkg-architecture -q DEB_BUILD_ARCH)
IMAGE_NAME=debian-${DEBIAN_RELEASE_NUM}-generic-${DEBIAN_ARCH}.qcow2
IMAGE_URL=http://cdimage.debian.org/cdimage/cloud/${DEBIAN_RELEASE}/latest/${IMAGE_NAME}
IMAGE_DOWNLOAD_TIMEOUT=30

####################
### GLANCE IMAGE ###
####################
oci_poc_prov_cloud_glance_image () {
	echo "===> Provisioning of a Glance image"
	if ! [ -f /root/${IMAGE_NAME} ] ; then
		echo "---> Fetching image from ${IMAGE_URL}"
		if [ -e /etc/oci/http_proxy_addr ] ; then
			http_proxy_addr=$(cat /etc/oci/http_proxy_addr)
			http_proxy=${http_proxy_addr} timeout ${IMAGE_DOWNLOAD_TIMEOUT} wget ${IMAGE_URL}
		else
			timeout ${IMAGE_DOWNLOAD_TIMEOUT} wget ${IMAGE_URL}
		fi
	else
		echo "---> Image already saved in /root"
	fi

	if [ -z "${IMAGE_NAME}" ] ; then
		echo "Cannot find image to upload to Glance"
		exit 1
	fi
	if echo "${IMAGE_NAME}" | grep -q qcow2 ; then
		DISK_FORMAT=qcow2
	else
		DISK_FORMAT=raw
	fi

	echo "---> Fetching image list"
	IMAGE_ID=$(openstack image list --property os_distro=debian --format value -c ID)

	if [ -z "${IMAGE_ID}" ] ; then
		echo "---> Uploading image to OpenStack"
		openstack image create \
			--property hw_disk_bus=scsi \
			--property hw_scsi_model=virtio-scsi \
			--container-format bare --disk-format ${DISK_FORMAT} \
			--property os_distro=debian \
			--file /root/${IMAGE_NAME} \
			--public \
			${IMAGE_NAME}
	fi
}

oci_poc_prov_cloud_glance_image

exit 0
