#!/bin/sh

# This script is a small helper to restart all OpenStack services
# on a given machine.

set -e

# List generated from a Git checkout of all OpenStack services
# packaged in Salsa: ~/salsa/openstack/services $ for i in $( ls */*/debian/*.init.in ) ; do echo $(basename $i | sed s/.init.in// | awk -F '.' '{print $NF}' ) ; done  | tr '\n' ' '
SERVICES_LIST="aodh-api aodh-evaluator aodh-expirer aodh-listener aodh-notifier barbican-api barbican-keystone-listener barbican-worker ceilometer-agent-central ceilometer-agent-compute ceilometer-agent-ipmi ceilometer-agent-notification ceilometer-polling cinder-api cinder-backup cinder-scheduler cinder-volume cloudkitty-api cloudkitty-processor congress-server designate-agent designate-api designate-central designate-mdns designate-pool-manager designate-producer designate-sink designate-worker designate-zone-manager distil-api distil-collector freezer-api freezer-scheduler glance-api glare-api gnocchi-api gnocchi-metricd gnocchi-statsd heat-api-cfn heat-api heat-engine ironic-inspector ironic-api ironic-conductor karbor-api karbor-operationengine karbor-protection keystone magnum-api magnum-conductor manila-api manila-data manila-scheduler manila-share masakari-api masakari-engine masakari-host-monitor masakari-instance-monitor masakari-introspective-instance-monitor masakari-process-monitor mistral-api mistral-engine mistral-event-engine mistral-executor monasca-api murano-agent murano-api murano-cfapi murano-engine neutron-api neutron-dhcp-agent neutron-l3-agent neutron-linuxbridge-agent neutron-macvtap-agent neutron-metering-agent neutron-ovn-metadata-agent neutron-rpc-server neutron-sriov-agent neutron-metadata-agent neutron-openvswitch-agent nova-api nova-api-metadata nova-conductor nova-novncproxy nova-serialproxy nova-spicehtml5proxy nova-scheduler nova-api nova-api-metadata nova-compute nova-conductor nova-novncproxy nova-serialproxy nova-spicehtml5proxy nova-scheduler octavia-agent octavia-api octavia-driver-agent octavia-health-manager octavia-housekeeping octavia-worker trove-api trove-conductor trove-guestagent trove-taskmanager panko-api placement-api sahara-api sahara-engine senlin-api senlin-conductor senlin-engine senlin-health-manager swift-account-auditor swift-account swift-account-reaper swift-account-replicator swift-container-auditor swift-container swift-container-reconciler swift-container-replicator swift-container-sharder swift-container-sync swift-container-updater swift-object-expirer swift-object-auditor swift-object swift-object-reconstructor swift-object-replicator swift-object-updater swift-proxy vitrage-api vitrage-collector vitrage-graph vitrage-ml vitrage-notifier vitrage-persistor vitrage-snmp-parsing watcher-api watcher-applier watcher-decision-engine zaqar-server"

for SERV in ${SERVICES_LIST} ; do
	# If a service doesn't exist, this will return 4.
	set +e
	systemctl status ${SERV} 2> /dev/null 1> /dev/null
	RET=$?
	set -e
	if [ "${RET}" = 4 ] ; then
		continue;
	fi
	echo "-> Restarting $SERV"
	systemctl restart $SERV
done

exit 0
