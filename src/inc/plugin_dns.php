<?php

function machine_create_host_dns($con, $conf, $machine, $ignore_dot_ini_activation='no'){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";

    if($ignore_dot_ini_activation=='yes' || $conf["dns_plugin"]["call_dns_shell_script"]){
        $q = "SELECT INET_NTOA( ips.ip ) AS ipaddr FROM ips,machines,networks WHERE machines.id='".$machine["id"]."' "
                ."AND ips.machine=machines.id AND ips.network=networks.id AND networks.role!='ipmi' "
                ."AND networks.role!='vm-net' AND networks.role!='vip' AND networks.is_public='no' AND networks.role!='ceph-cluster'";
        $r = mysqli_query($con, $q);
        if($r === FALSE){
            $json["status"] = "error";
            $json["message"] = mysqli_error($con). " doing $q";
            return $json;
        }
        $n = mysqli_num_rows($r);
        if($n == 1){
            $ip = mysqli_fetch_array($r);
            $ipaddr = $ip["ipaddr"];
            $script_path = $conf["dns_plugin"]["dns_create_script_path"];
            if(file_exists ($script_path) && is_executable ($script_path) ){
                $cmd = "$script_path --hostname ".$machine["hostname"]." --ip-address $ipaddr";
                $output = array();
                $return_var = 0;
                exec($cmd, $output, $return_var);
                $json["data"]["command"] = $cmd;
                $json["data"]["output"] = implode("\n", $output);
                return $json;
            }
        }
    }
}

function machine_delete_host_dns($con, $conf, $machine, $ignore_dot_ini_activation='no'){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";
    if($ignore_dot_ini_activation=='yes' || $conf["dns_plugin"]["call_dns_shell_script"]){
        $q = "SELECT INET_NTOA( ips.ip ) AS ipaddr FROM ips,machines,networks WHERE machines.id='".$machine["id"]."' "
                ."AND ips.machine=machines.id AND ips.network=networks.id AND networks.role!='ipmi' "
                ."AND networks.role!='vm-net' AND networks.role!='vip' AND networks.is_public='no' AND networks.role!='ceph-cluster'";
        $r = mysqli_query($con, $q);
        if($r === FALSE){
            $json["status"] = "error";
            $json["message"] = mysqli_error($con). " doing $q";
            return $json;
        }
        $n = mysqli_num_rows($r);
        if($n == 1){
            $ip = mysqli_fetch_array($r);
            $ipaddr = $ip["ipaddr"];
            $script_path = $conf["dns_plugin"]["dns_delete_script_path"];
            if(file_exists ($script_path) && is_executable ($script_path) ){
                $cmd = "$script_path --hostname ".$machine["hostname"]." --ip-address $ipaddr";
                $output = array();
                $return_var = 0;
                exec($cmd, $output, $return_var);
                $json["data"]["command"] = $cmd;
                $json["data"]["output"] = implode("\n", $output);
                return $json;
            }
        }
    }
}

?>